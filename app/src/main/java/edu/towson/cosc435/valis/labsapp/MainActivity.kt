import android.graphics.Color
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.ui.graphics.Color.Companion.Red
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import java.lang.reflect.Modifier

import android.os.Bundle
import android.text.Layout
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.platform.textInputServiceFactory
import androidx.compose.ui.tooling.preview.Preview
import edu.towson.cosc435.valis.labsapp.ui.theme.LabsAppTheme
import org.intellij.lang.annotations.JdkConstants

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LabsAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Greeting("Android")
                }
            }
        }
    }
}
@Composable
fun MainScreen()
{
    Column (
        horizontalAlignment = Alignment.CenterHorizontally,
        veritcalAlignment = Alignment.SpaceBetween,
        modifier = Modifier
            .border(2.dp, Color.RED)
            .fill
            )
    {
        Text(text = "Second Lab", fontSize = 30.sp)
        CourseInfoRow()
        CourseNumberRow()
    }
}

@Composable
fun CourseInfoRow() {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center
    ) {
        Text(text = "Course Name:")
        Text(text = "Mobile App Development")
    }
}

@Composable
fun CourseNumberRow() {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center
    ) {
        Text(text = "Course Name:")
        Text(text = "Mobile App Development")
    }
}
@Composable
fun ButtonRow() {
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceEvenly

    ) {
        Button(onClick = {})
        {
            Text(text = "Yes")
        }
        Button(onClick = {})
        {
            Text(text = "No")
        }
        Button(onClick = {})
        {
            Text(text = "Cancel")
        }
    }
}
@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    LabsAppTheme {
        Greeting("Android")
    }
}